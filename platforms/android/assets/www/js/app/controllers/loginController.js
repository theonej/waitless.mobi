﻿var loginController = function ($scope, authServices, baseApiUrl) {

    $scope.user = {};
    $scope.loading = false;

    $scope.authenticateUser = function () {
       
        $scope.loading = true;
        authServices.authenticateUser($scope.user, $scope.onAuthenticated, $scope.onError);
    };

    $scope.onAuthenticated = function (data) {
        console.log(data);

        $scope.loading = false;
    };

    $scope.onError = function (error) {
        console.log('ERR:');
        console.log(error);

        $scope.loading = false;
    };
};

waitless.controller('loginController', [
                                        '$scope',
                                        'authServices',
                                        'baseApiUrl',
                                        loginController
                                    ]);