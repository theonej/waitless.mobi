﻿waitless = angular.module('waitless', ['ngRoute']);

waitless.value('baseApiUrl', 'http://api.waitless.mobi/api/');

waitless.config(['$routeProvider',
                  function ($routeProvider) {
                      $routeProvider.
                      when('/', {
                          templateUrl: '/templates/login.html',
                          controller:'loginController'
                      })
                      .otherwise({
                          redirectTo:'/orders'
                      });
                  }]);