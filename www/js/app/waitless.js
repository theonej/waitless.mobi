﻿waitless = angular.module('waitless', ['ngRoute', 'ngCookies']);

waitless.value('baseApiUrl', 'https://api.waitless.mobi/api/');
//waitless.value('baseApiUrl', 'http://localhost:49555/api/');

waitless.config(['$routeProvider',
                  function ($routeProvider) {
                      $routeProvider.
                      when('/', {
                          templateUrl: 'templates/login.html',
                          controller:'loginController'
                      }).
                      when('/404', {
                          templateUrl: 'templates/404.html'
                      }).
                      when('/reset', {
                          templateUrl: 'templates/account/reset.html',
                          controller: 'resetPasswordController'
                      }).
                      when('/passwordResetConfirmation', {
                          templateUrl: 'templates/account/passwordResetConfirmation.html'
                      }).
                      when('/register', {
                          templateUrl: 'templates/account/register.html',
                          controller: 'registerController'
                      }).
                      when('/registrationConfirmation', {
                          templateUrl: 'templates/account/registrationConfirmation.html'
                      }).
                      when('/setPassword', {
                          templateUrl: 'templates/account/setPassword.html',
                          controller:'setPasswordController'
                      }).
                      when('/serviceProviders', {
                          templateUrl: 'templates/providers/serviceProviders.html',
                          controller: 'serviceProvidersController'
                      }).
                      when('/addLocation', {
                          templateUrl: 'templates/providers/addLocation.html',
                          controller:'addLocationController'
                      }).
                      when('/editServiceProvider/:providerId', {
                          templateUrl: 'templates/providers/editServiceProvider.html',
                          controller:'editProviderController'
                      }).
                      when('/providers/:providerId', {
                          templateUrl: 'templates/providers/providerDetails.html',
                          controller: 'providerDetailsController'
                      }).
                      when('/providers/:providerId/orders', {
                          templateUrl: 'templates/order/providerOrders.html',
                          controller: 'providerOrdersController'
                      }).
                      when('/providers/:providerId/orders/:orderId', {
                          templateUrl: 'templates/order/orderDetails.html',
                          controller: 'orderDetailsController'
                      }).
                      when('/providers/:providerId/addInventoryItem', {
                          templateUrl: 'templates/service/addInventoryItem.html',
                          controller: 'addInventoryItemController'
                      }).
                      when('/providers/:providerId/inventoryItems/:inventoryItemId/edit', {
                          templateUrl: 'templates/service/editInventoryItem.html',
                          controller: 'editInventoryItemController'
                      })
                      .otherwise({
                          redirectTo:'/login'
                      });
                  }]);