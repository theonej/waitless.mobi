﻿var registrationServices = function ($http, baseApiUrl) {
    return {
        registerServiceProvider: function (registrationInfo, success, failure) {

            var url = baseApiUrl + 'ServiceProviders/';

            var command = {
                id: '00000000-0000-0000-0000-000000000000',
                serviceProviderId: '00000000-0000-0000-0000-000000000000',
                providerName: registrationInfo.name,
                primaryUserEmailAddress: registrationInfo.emailAddress
            };

            var config = {
                url: url,
                data:command
            };

            $http.post(url, command, config)
                .success(success)
                .error(failure);
        }
    };
};

waitless.factory('registrationServices', ['$http', 'baseApiUrl', registrationServices]);