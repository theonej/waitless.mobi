﻿var serviceProviderServices = function ($http, $cookies, baseApiUrl, userServices) {
    return {
        getServiceProviders: function (success, failure) {

            var headers = {
                'WaitlessAuthToken': $cookies.authToken
            };

            var url = baseApiUrl + 'ServiceProviders/';

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(success)
                .error(failure);
        },

        getProvider: function (providerId, success, failure) {

            var headers = {
                'WaitlessAuthToken': $cookies.authToken
            };

            var url = baseApiUrl + 'ServiceProviders/' + providerId;

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(success)
                .error(failure);
        },

        addChildLocation: function (location, success, failure) {

            var headers = {
                'WaitlessAuthToken': $cookies.authToken
            };

            var url = baseApiUrl + 'ServiceProviders/';

            var command = {
                id: '00000000-0000-0000-0000-000000000000',
                serviceProviderId: '00000000-0000-0000-0000-000000000000',
                providerName: location.name,
                parentId: location.parentId
            };

            var config = {
                url: url,
                data: command,
                headers: headers
            };

            $http.post(url, command, config)
                .success(success)
                .error(failure);
        },

        updateProvider: function (provider, success, failure) {
            var headers = {
                'WaitlessAuthToken': $cookies.authToken
            };

            var url = baseApiUrl + 'ServiceProviders/';

            var command = {
                id: '00000000-0000-0000-0000-000000000000',
                serviceProviderId: provider.id,
                description:provider.description,
                name: provider.name,
                parentId: provider.parentId,
                location: provider.location,
                phoneNumber: provider.phoneNumber,
                updatedBy: userServices.getCurrentUser().emailAddress,
                stateSalesTaxPercentage: provider.stateSalesTaxPercentage,
                localSalesTaxPercentage: provider.localSalesTaxPercentage,
                orderNotifications: provider.orderNotifications
            };

            var config = {
                url: url,
                data: command,
                headers: headers
            };

            $http.put(url, command, config)
                .success(success)
                .error(failure);
        },

        deleteProvider: function (providerId, success, failure) {
            var headers = {
                'WaitlessAuthToken': $cookies.authToken
            };

            var url = baseApiUrl + 'ServiceProviders/' + providerId;

            var config = {
                url: url,
                headers: headers
            };

            $http.delete(url, config)
                .success(success)
                .error(failure);
        }
    };
};

waitless.factory('serviceProviderServices', ['$http', '$cookies', 'baseApiUrl', 'userServices', serviceProviderServices]);