﻿var configurationServices = function ($http, baseApiUrl) {
    return {
        getCountries: function () {
            //hard coded for now
            return ['United States' ];
        },

        getStates: function (countryName) {
            //hard coded for now
            if(countryName === 'United States')
            return [
            'Alabama',
            'Alaska',
            'Arizona',
            'Arkansas',
            'California',
            'Colorado',
            'Connecticut',
            'Delaware',
            'Florida',
            'Georgia',
            'Hawaii',
            'Idaho',
            'Illinois',
            'Indiana',
            'Iowa',
            'Kansas',
            'Kentucky',
            'Louisiana',
            'Maine',
            'Maryland',
            'Massachusetts',
            'Michigan',
            'Minnesota',
            'Mississippi',
            'Missouri',
            'Montana',
            'Nebraska',
            'Nevada',
            'New Hampshire',
            'New Jersey',
            'New Mexico',
            'New York',
            'North Carolina',
            'North Dakota',
            'Ohio',
            'Oklahoma',
            'Oregon',
            'Pennsylvania',
            'Rhode Island',
            'South Carolina',
            'South Dakota',
            'Tennessee',
            'Texas',
            'Utah',
            'Vermont',
            'Virginia',
            'Washington',
            'West Virginia',
            'Wisconsin',
            'Wyoming',
            'District of Columbia',
            'Puerto Rico',
            'Guam',
            'American Samoa',
            'U.S. Virgin Islands',
            'Northern Mariana Islands'
        ];
        }
    };
};

waitless.factory('configurationServices', ['$http', 'baseApiUrl', configurationServices]);