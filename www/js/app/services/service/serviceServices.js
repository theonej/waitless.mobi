﻿var serviceServices = function ($http, baseApiUrl, $cookies, userServices) {
    return {
        getServiceCategories: function (success, failure) {
            var headers = {
                'WaitlessAuthToken': $cookies.authToken
            };

            var url = baseApiUrl + 'ServiceCategory/';

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(success)
                .error(failure);
        },

        getServiceInventory: function (providerId, success, failure) {
            var headers = {
                'WaitlessAuthToken': $cookies.authToken
            };

            var url = baseApiUrl + 'ServiceProvider/' + providerId + '/ServiceInventory/';

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(success)
                .error(failure);
        },

        getInventoryItem: function (providerId, inventoryItemId, success, failure) {
            var headers = {
                'WaitlessAuthToken': $cookies.authToken
            };

            var url = baseApiUrl + 'ServiceProvider/' + providerId + '/ServiceInventory/' + inventoryItemId;

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(success)
                .error(failure);
        },

        addInventoryItem: function (providerId, item, success, failure) {
            var headers = {
                'WaitlessAuthToken': $cookies.authToken
            };

            var url = baseApiUrl + 'ServiceProvider/' + providerId + '/ServiceInventory/';

            var command = {
                id: '00000000-0000-0000-0000-000000000000',
                serviceProviderId:providerId,
                categoryId: item.category.id,
                name: item.name,
                description: item.description,
                minimumAgeRestriction: item.minimumAgeRestriction,
                PriceDollarsUS: item.priceDollarsUs,
                itemCount: item.itemCount,
                createdBy: userServices.getCurrentUser().emailAddress
            };

            var config = {
                url: url,
                data: command,
                headers: headers
            };

            $http.post(url, command, config)
                .success(success)
                .error(failure);
        },

        updateInventoryItem: function (providerId, item, success, failure) {
            var headers = {
                'WaitlessAuthToken': $cookies.authToken
            };

            var url = baseApiUrl + 'ServiceProvider/' + providerId + '/ServiceInventory/';

            var command = {
                id: '00000000-0000-0000-0000-000000000000',
                inventoryItemId: item.id,
                categoryId: item.category.id,
                name: item.name,
                description: item.description,
                minimumAgeRestriction: item.minimumAgeRestriction,
                priceDollarsUS: item.priceDollarsUS,
                itemCount: item.itemCount,
                updatedBy: userServices.getCurrentUser().emailAddress
            };

            var config = {
                url: url,
                data: command,
                headers: headers
            };

            $http.put(url, command, config)
                .success(success)
                .error(failure);
        },

        deleteInventoryItem: function (providerId, inventoryItemId, success, failure) {
            var headers = {
                'WaitlessAuthToken': $cookies.authToken
            };

            var url = baseApiUrl + 'ServiceProvider/' + providerId + '/ServiceInventory/' + inventoryItemId;

            var config = {
                url: url,
                headers: headers
            };

            $http.delete(url, config)
                .success(success)
                .error(failure);
        }
    };
};

waitless.factory('serviceServices', ['$http', 'baseApiUrl', '$cookies', 'userServices', serviceServices]);