﻿var orderServices = function ($http, $cookies, baseApiUrl) {
    return {
        getProviderOrders: function (providerId, success, failure) {
            var headers = {
                'WaitlessAuthToken': $cookies.authToken
            };

            var url = baseApiUrl + 'ServiceProvider/' + providerId + '/Service/';

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(success)
                .error(failure);
        },

        getOrderDetails: function (orderId, success, failure) {
            var headers = {
                'WaitlessAuthToken': $cookies.authToken
            };

            var url = baseApiUrl + 'Service/' + orderId;

            var config = {
                url: url,
                headers: headers
            };

            $http.get(url, config)
                .success(success)
                .error(failure);
        },

        updateOrder: function (providerId, order, success, failure) {
            var url = baseApiUrl + '/ServiceProvider/' + providerId + '/Service/';

            var headers = {
                'WaitlessAuthToken': $cookies.authToken
            };

            var command = {
                Id: '00000000-0000-0000-0000-000000000000',
                ServiceId: order.id,
                Status: order.status
            };

            var config = {
                data: command,
                url: url,
                type: 'PUT',
                headers: headers
            };

            $http.put(url, command, config)
                .success(success)
                .error(failure);
        }
    };
};

waitless.factory('orderServices', ['$http', '$cookies', 'baseApiUrl', orderServices]);