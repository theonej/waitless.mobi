﻿var orderDetailsController = function ($scope, $location, $routeParams, orderServices) {
    controllerBase.apply(this, [$scope, $location]);

    $scope.providerId = $routeParams.providerId;
    $scope.orderId = $routeParams.orderId;
    $scope.order = null;

    $scope.initialize = function () {
        $scope.loading = true;

        orderServices.getOrderDetails($scope.orderId, $scope.onOrderDetails, $scope.onError);
    };

    $scope.onOrderDetails = function (data) {
        $scope.order = data;

        $scope.loading = false;
    };

    $scope.updateOrderStatus = function (status) {
        $scope.loading = true;

        $scope.order.status = status;
        orderServices.updateOrder($scope.providerId, $scope.order, $scope.onOrderUpdated, $scope.onError);
    };

    $scope.onOrderUpdated = function (data) {
        $scope.displayMessage('Order status updated successfully');
        $location.path('/providers/' + $routeParams.providerId + '/orders/');
    };
};

waitless.controller('orderDetailsController', ['$scope', '$location', '$routeParams', 'orderServices', orderDetailsController]);