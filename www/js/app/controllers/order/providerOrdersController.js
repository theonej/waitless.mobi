﻿var providerOrdersController = function ($scope, $location, $routeParams, orderServices) {
    controllerBase.apply(this, [$scope, $location]);

    $scope.providerId = $routeParams.providerId;
    $scope.orders = null;

    $scope.initialize = function () {
        $scope.loading = true;

        orderServices.getProviderOrders($scope.providerId, $scope.onOrders, $scope.onError);
    };

    $scope.onOrders = function (data) {
        $scope.orders = data;
        $scope.loading = false;
    };
};

waitless.controller('providerOrdersController', ['$scope', '$location', '$routeParams', 'orderServices', providerOrdersController]);