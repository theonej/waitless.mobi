﻿var editInventoryItemController = function ($scope, $location, $routeParams, serviceServices) {
    controllerBase.apply(this, [$scope, $location]);

    $scope.providerId;
    $scope.item = null;
    $scope.serviceCategories = null;
    $scope.initialize = function () {
        $scope.loading = true;

        $scope.providerId = $routeParams.providerId;
        serviceServices.getInventoryItem($routeParams.providerId, $routeParams.inventoryItemId, $scope.onInventoryItem, $scope.onError);
        serviceServices.getServiceCategories($scope.onServiceCategories, $scope.onError);
    };

    $scope.onInventoryItem = function (data) {
    
        $scope.item = {
            id: data.id,
            category: data.category,
            name: data.itemType.name,
            description: data.itemType.description,
            minimumAgeRestriction: data.itemType.minimumAgeRestriction,
            priceDollarsUS: data.itemType.priceDollarsUS,
            itemCount: data.itemCount
        };

        $scope.loading = false;
    };

    $scope.onServiceCategories = function (data) {
        $scope.serviceCategories = data;
    };

    $scope.updateInventoryItem = function () {
        $scope.loading = true;
        serviceServices.updateInventoryItem($scope.providerId, $scope.item, $scope.onItemUpdated, $scope.onError);
    };

    $scope.onItemUpdated = function (data) {
        $scope.loading = false;

        $scope.displayMessage('Item updated successfully');
        $location.path('/providers/' + $scope.providerId);
    };
};

waitless.controller('editInventoryItemController', ['$scope', '$location', '$routeParams', 'serviceServices', editInventoryItemController]);