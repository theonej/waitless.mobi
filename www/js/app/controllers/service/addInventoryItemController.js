﻿var addInventoryItemController = function ($scope, $location, $routeParams, serviceServices) {
    controllerBase.apply(this, [$scope, $location]);

    $scope.providerId;

    $scope.serviceCategories = null;
    $scope.item = {
        minimumAgeRestriction: 0,
        priceDollarsUs: 0.00,
        itemCount:0
    };

    $scope.initialize = function () {
        $scope.loading = true;
        $scope.providerId = $routeParams.providerId;

        serviceServices.getServiceCategories($scope.onServiceCategories, $scope.onError);
    };

    $scope.onServiceCategories = function (data) {
        $scope.serviceCategories = data;
        $scope.loading = false;
    };

    $scope.addInventoryItem = function () {
        serviceServices.addInventoryItem($routeParams.providerId, $scope.item, $scope.onItemAdded, $scope.onError);
    };

    $scope.onItemAdded = function (data) {
        $scope.displayMessage('Item added successfully');
        $location.path('/providers/' + $routeParams.providerId);
    };
};

waitless.controller('addServiceItemController', ['$scope', '$location', '$routeParams', 'serviceServices', addInventoryItemController]);