﻿var providerDetailsController = function ($scope, $location, $routeParams, serviceProviderServices, serviceServices) {

    controllerBase.apply(this, [$scope, $location]);

    $scope.provider = {};
    $scope.providerInventoryItems = null;

    $scope.initialize = function () {
        $scope.loading = true;

        serviceProviderServices.getProvider($routeParams.providerId, $scope.onProvider, $scope.onError);
        serviceServices.getServiceInventory($routeParams.providerId, $scope.onServices, $scope.onError);
    };

    $scope.onProvider = function (data) {
        $scope.provider = data;

        $scope.loading = false;
    };

    $scope.onServices = function (data) {
        $scope.providerInventoryItems = data;
    };

    $scope.delete = function (inventoryItem) {
        var confirmed = confirm('Are you sure you want to delete this item?  This cannot be undone.');
        if (confirmed) {
            $scope.loading = true;
            serviceServices.deleteInventoryItem($scope.provider.id, inventoryItem.id, $scope.onItemDeleted, $scope.onError);
        }
    };

    $scope.onItemDeleted = function () {
        $scope.initialize();
        $scope.loading = false;
    };
};

waitless.controller('providerDetailsController', ['$scope',
                                                  '$location',
                                                  '$routeParams', 
                                                  'serviceProviderServices',
                                                  'serviceServices',
                                                  providerDetailsController]);