﻿var addLocationController = function ($scope, $location, userServices, serviceProviderServices, configurationServices) {
    controllerBase.apply(this, [$scope, $location]);
    $scope.provider = {};
    $scope.address = {country:''};

    $scope.states = [];
    $scope.countries = configurationServices.getCountries();
    $scope.$watch('address.country', function (selectedCountry, oldValue) {
        $scope.states = configurationServices.getStates(selectedCountry);
    });

    $scope.addLocation = function () {
        var currentUser = userServices.getCurrentUser();
        $scope.provider.parentId = currentUser.serviceProviderId;

        $scope.loading = true;

        serviceProviderServices.addChildLocation($scope.provider, $scope.onLocationAdded, $scope.onError);
    };

    $scope.onLocationAdded = function (data) {
        $scope.provider.id = data;
        $scope.provider.location = $scope.address;

        serviceProviderServices.updateProvider($scope.provider, $scope.onProviderUpdated, $scope.onError);
    };

    $scope.onProviderUpdated = function (data) {
        $scope.loading = false;
        $scope.displayMessage('New location added successfully');
        $location.path('/serviceProviders');
    };
};

waitless.controller('addLocationController', ['$scope', '$location', 'userServices', 'serviceProviderServices', 'configurationServices', addLocationController]);