﻿var editProviderController = function ($scope, $location, $routeParams, configurationServices, serviceProviderServices) {
    controllerBase.apply(this, [$scope, $location]);

    $scope.countries = configurationServices.getCountries();
    $scope.selectedTab = 'info';

    $scope.initialize = function () {
        $scope.loading = true;
        serviceProviderServices.getProvider($routeParams.providerId, $scope.onProvider, $scope.onError);
        $scope.$watch('provider.location.country', function (selectedCountry, oldValue) {
            $scope.states = configurationServices.getStates(selectedCountry);
        });
    };

    $scope.onProvider = function (data) {
        $scope.provider = data;
        console.log(data);
        var types =[];
        types[0] ='email';
        types[1] = 'sms';

        if ($scope.provider.orderNotifications === undefined || $scope.provider.orderNotifications === null) {
            $scope.provider.orderNotifications = [];
        }
        angular.forEach($scope.provider.orderNotifications, function (notification, key) {
            notification.notificationType = types[notification.notificationType];
        });

        if ($scope.provider.stateSalesTaxPercentage === undefined) {
            $scope.provider.stateSalesTaxPercentage = 0.00;
        }

        if ($scope.provider.localSalesTaxPercentage === undefined) {
            $scope.provider.localSalesTaxPercentage = 0.00;
        }
        $scope.loading = false;
    };

    $scope.updateProvider = function () {
        serviceProviderServices.updateProvider($scope.provider, $scope.onProviderUpdated, $scope.onError);
    };

    $scope.onProviderUpdated = function (data) {
        $scope.loading = false;
        $scope.displayMessage('Location updated successfully');
        $location.path('/serviceProviders');
    };

    $scope.addNotification = function () {

        $scope.provider.orderNotifications.push({ notificationType: 'email', address: '' });
    };

    $scope.tabIsSelected = function(tabName){
        return $scope.selectedTab === tabName;
    };

    $scope.selectTab = function (tabName) {
        $scope.selectedTab = tabName;
    };

    $scope.removeNotification = function (index) {
        $scope.provider.orderNotifications.splice(index, 1);
    };
};

waitless.controller('editProviderController', ['$scope', '$location', '$routeParams', 'configurationServices', 'serviceProviderServices', editProviderController]);