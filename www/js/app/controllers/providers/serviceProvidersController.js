﻿var serviceProvidersController = function ($scope, $location, serviceProviderServices) {
    controllerBase.apply(this, [$scope, $location]);

    $scope.providers = [];

    $scope.initialize = function () {
        $scope.loading = true;
        serviceProviderServices.getServiceProviders($scope.onServiceProviders, $scope.onError);
    };

    $scope.onServiceProviders = function (data) {
        $scope.providers = data;
        $scope.loading = false;
    };

    $scope.edit = function (provider) {
        $location.path('/editServiceProvider/' + provider.id);
    };

    $scope.delete = function (provider) {
        var confirmed = confirm('Are you sure you want to delete this item?  This cannot be undone.');
        if (confirmed) {
            $scope.loading = true;
            serviceProviderServices.deleteProvider(provider.id, $scope.onProviderDeleted, $scope.onError);
        }
    };

    $scope.onProviderDeleted = function () {
        $scope.initialize();
    };
};

waitless.controller('serviceProvidersController', ['$scope', '$location', 'serviceProviderServices',  serviceProvidersController]);