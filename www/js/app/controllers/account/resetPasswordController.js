﻿var resetPasswordController = function ($scope, $location, userServices) {
    controllerBase.apply(this, [$scope, $location]);

    $scope.emailAddress = null;

    $scope.resetPassword = function () {
        $scope.loading = true;
        userServices.resetPassword($scope.emailAddress, $scope.onPasswordReset, $scope.onError);
    };

    $scope.onPasswordReset = function (data) {
        $scope.loading = false;

        $scope.displayMessage('Password Reset Successful!');
        $location.path('/passwordResetConfirmation');
    };
};

waitless.controller('resetPasswordController', ['$scope', '$location', 'userServices', resetPasswordController]);