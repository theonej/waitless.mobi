﻿var registerController = function ($scope, $location, registrationServices) {

    controllerBase.apply(this, [$scope, $location]);

    $scope.registrationInfo = {
        emailAddress: '',
        name:'',
        valid: function () {
            var valid = false;

            if (this.emailAddress != '' && this.name != '') {
                valid = true;
            }

            return valid;
        }
    };

    $scope.register = function () {
        if ($scope.registerForm.$valid === true && $scope.registrationInfo.valid() === true) {

            $scope.loading = true;

            registrationServices.registerServiceProvider($scope.registrationInfo, $scope.onProviderRegistered, $scope.onError);
        }
    };

    $scope.onProviderRegistered = function (data) {
        $scope.loading = false;

        $scope.displayMessage('Registration Successful!');
        $location.path('/registrationConfirmation');
    };
};

waitless.controller('registerController', ['$scope', '$location', 'registrationServices', registerController]);