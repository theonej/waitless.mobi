﻿var setPasswordController = function ($scope, $rootScope, $location, $cookies, userServices, authServices) {
    controllerBase.apply(this, [$scope, $location]);

    $scope.token = null;
    $scope.userData = null;
    $scope.password = null;
    $scope.passwordConfirmation = null;

    $scope.initialize = function () {
        $scope.token = $location.search().token;
        
        $scope.loading = true;
        userServices.getUser($scope.token, $scope.onUserData, $scope.onError);
    };

    $scope.onUserData = function (data) {
        $scope.userData = data;

        $rootScope.$broadcast('userDataLoaded', data);

        $scope.loading = false;
    };

    $scope.setPassword = function () {
        if ($scope.validatePassword() === true && $scope.setPasswordForm.$valid === true) {
            $scope.loading = true;
            $scope.userData.password = $scope.password;
            userServices.setPassword($scope.userData, $scope.token, $scope.onPasswordSet, $scope.onError);
        }
    };

    $scope.onPasswordSet = function (data) {
        //log the user in
        $scope.displayMessage('Password set.  Logging you in now.');
        authServices.authenticateUser($scope.userData, $scope.onAuthenticated, $scope.onError);
    };

    $scope.onAuthenticated = function (data) {
        $cookies.authToken = data;

        $scope.loading = false;

        $location.path('/serviceProviders');
    };

    $scope.validatePassword = function () {
        var valid = true;

        if($scope.password !== $scope.passwordConfirmation){
            valid = false;
            $scope.displayError('Password and confirmation must match');
        }

        if($scope.password === null || $scope.password === ''){
            valid = false;
            $scope.displayError('Password must not be empty');
        }

        $scope.password = $scope.password || '';

        if($scope.password.length < 8){
            valid = false;
            $scope.displayError('Password must be at least 8 characters');
        }

           
        return valid;
    };
};

waitless.controller('setPasswordController', ['$scope', '$rootScope', '$location', '$cookies', 'userServices', 'authServices', setPasswordController]);