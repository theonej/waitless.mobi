﻿var headerController = function ($scope, $rootScope, $cookies, $location, userServices) {
    controllerBase.apply(this, [$scope, $location]);

    $scope.userDataLoaded = false;

    $scope.initialize = function () {
        if ($scope.userDataLoaded === false && $cookies.authToken !== undefined) {
            $scope.loading = true;
            userServices.getUser($cookies.authToken, $scope.onUserData, $scope.onError);
        } else {
            if (!$location.search().token) {
                $location.path('/');
            }
        }
    };

    $scope.onUserData = function (data) {
        userServices.setCurrentUser(data);
        $scope.userDataLoaded = true;
        $scope.loading = false;
    };

    $scope.$on('userDataLoaded', function (event, data) {
        userServices.setCurrentUser(data);
        $scope.userDataLoaded = true;
    });

    $scope.logOut = function () {
        $cookies.authToken = undefined;
        userServices.setCurrentUser(null);
        $scope.userDataLoaded = false;
        $location.path('/');
    };
};

waitless.controller('headerController', ['$scope', '$rootScope', '$cookies', '$location', 'userServices', headerController]);